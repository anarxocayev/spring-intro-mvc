package com.example.jalil.demojalil.controller;

import com.example.jalil.demojalil.dao.entity.Content;
import com.example.jalil.demojalil.dao.repository.ContentRepository;
import com.example.jalil.demojalil.mapper.ContentMapper;
import com.example.jalil.demojalil.model.ContentModelDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@AllArgsConstructor
@RequestMapping("/management")
@Slf4j
public class MngmntController {
    private ContentRepository contentRepository;


    @GetMapping("/add")
    public String getForm(Model model) {
        List<SubMenu> subMenuList =List.of(new SubMenu(1L,"Basics "),
                new SubMenu(2L,"Loops"),new SubMenu(3l,"Operators and Keywords "));

        model.addAttribute("content",new ContentModelDto());
        model.addAttribute("submenu",subMenuList);

        return "content-form";
    }

    @PostMapping("/save")
    @ResponseBody
    public ModelAndView saveContent(@ModelAttribute ContentModelDto contentModelDto) {
        log.info("content model dto {} ",contentModelDto);
        Content content = ContentMapper.INSTANCE.mapContentDtoToContent(contentModelDto);
        log.info("content {} ",content);
        log.info("new content is {} ",contentRepository.save(content));
        return new ModelAndView("redirect:" + "/");
    }


}

@Data
@AllArgsConstructor
class SubMenu{

    long id;
    String name;


}
