package com.example.jalil.demojalil.controller;

import com.example.jalil.demojalil.dao.entity.Content;
import com.example.jalil.demojalil.dao.repository.ContentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.util.StringUtils;

@Controller
@AllArgsConstructor
public class MainController {
    private ContentRepository contentRepository;

    @GetMapping({"/","home",})
    public ModelAndView  home(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.addObject("contents",contentRepository.findAll());
        modelAndView.setViewName("home");
        return modelAndView;
    }
    @GetMapping("/content/{id}")
    public ModelAndView  contentView(@PathVariable Long id){
        ModelAndView modelAndView=new ModelAndView();

       Content content = contentRepository.findById(id).get()  ;
    //   content.setContent(StringUtils.escapeXml(content.getContent()));

        modelAndView.addObject("content",content);
        modelAndView.setViewName("content-view");
        return modelAndView;
    }

}

//https://www.w3schools.com/bootstrap4/bootstrap_forms_inputs.asp
