package com.example.jalil.demojalil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoJalilApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoJalilApplication.class, args);
    }

}
