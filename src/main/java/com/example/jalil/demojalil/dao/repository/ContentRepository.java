package com.example.jalil.demojalil.dao.repository;

import com.example.jalil.demojalil.dao.entity.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository  extends JpaRepository<Content,Long> {
}
