package com.example.jalil.demojalil.model;

import lombok.Data;

@Data
public class ContentModelDto {

   private String title;
   private String content;
   private String author;
}


