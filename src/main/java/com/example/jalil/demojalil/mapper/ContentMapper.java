package com.example.jalil.demojalil.mapper;

import com.example.jalil.demojalil.dao.entity.Content;
import com.example.jalil.demojalil.model.ContentModelDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
public abstract  class ContentMapper {

    public static final ContentMapper INSTANCE = Mappers.getMapper(ContentMapper.class);
    @Mappings({
            @Mapping(target = "title",source = "title"),
            @Mapping(target = "content", source = "content"),
            @Mapping(target = "author", source = "author")

    })
    public abstract Content mapContentDtoToContent(ContentModelDto contentModelDto);

}
